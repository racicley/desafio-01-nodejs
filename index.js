const express = require('express');
const cors = require('cors');

const { v4: uuidv4 } = require('uuid');

const app = express();

app.use(cors());
app.use(express.json());

const users = [];

function checksExistsUserAccount(request, response, next) {
    const {username} = request.headers;
    
    const user = users.find( (u)=> u.username == username);

    if(!user){
        return response.status(400).json({ error: "Usuário não encontrado!"});
    }
    
    request.user = user;

    return next();
}

function checkUserName(request, response, next){
    const {username} = request.headers;

    const user = users.find( (u)=> u.username == username);

    //undefined === não criado
    if(user != undefined){
        return response.status(403).json({msg:"Usuário já criado!"});
    }else{
        next()
    }
}

app.post('/users', checkUserName, (request, response) => {
    const {username} = request.headers;
    const {name} = request.body;

    users.push({ 
        id: uuidv4(), 
        name: name, 
        username: username, 
        todos: []
        });

    return response.status(201).json(users);
});

app.get('/todos', checksExistsUserAccount, (request, response) => {
    const {user} = request;
    
    return response.status(201).json(user);
});

app.post('/todos', checksExistsUserAccount, (request, response) => {
    const {user} = request;
    const{title, deadline} = request.body;

    user.todos.push({ 
        id: uuidv4(), // precisa ser um uuid
        title: title,
        done: false, 
        deadline: deadline, 
        created_at: new Date()
    });

    return response.status(201).json(user.todos);
});

app.put('/todos/:id', checksExistsUserAccount, (request, response) => {
    const {user} = request;
    const {id} = request.params;
    const {deadline, title} = request.body;

    user.todos.forEach(tarefa => {
        if(tarefa.id === id){
            if(deadline != undefined){
                tarefa.deadline = deadline;
            }
            if(title != undefined){
                tarefa.title = title;
            }
        }
    });

    return response.status(201).json(user);
});

app.patch('/todos/:id/done', checksExistsUserAccount, (request, response) => {
    const {user} = request;
    const {id} = request.params;

    user.todos.forEach(tarefa => {
        if(tarefa.id === id){
            tarefa.done = true
        }
    });

    return response.status(201).json(user);
});

app.delete('/todos/:id', checksExistsUserAccount, (request, response) => {
    const {user} = request;
    const {id} = request.params;

    const tarefaIndex = user.todos.findIndex((tarefa) => tarefa.id === id);

    user.todos.splice(tarefaIndex,1);

    return response.status(201).json(users);
});

app.listen(3000);